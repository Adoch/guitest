if [ "$(id -u)" = 0 ]; then
	echo "Do not run as root or sudo"
	exit 1
fi

sudo pacman --noconfirm --needed -S sddm plasma xorg wayland plasma-wayland-session kde-applications base-devel nautilus konsole packagekit packagekit-qt5
sudo systemctl enable sddm.service
cd /opt
sudo git clone https://aur.archlinux.org/yay.git
sudo chown -R $USER:users ./yay
cd yay
makepkg -si --noconfirm
yay -S brave-bin --noconfirm
sudo reboot 0
